# README #

### Author

Name: Ian Parish  
Email: iparish@cs.uoregon.edu  

### Page Server ###

This program creates a small web server. It will serve pages if they
exist within the specified directory, display "403 Forbidden" if the url
contains "~", "//", or "..". If the page does not exist within the specified
directory, it will serve "404 Not Found".  

#### Additional Information ####
This project was developed for CIS322 F20, University of Oregon.  
Instructor: Ram Durairajan

The objectives of this project are:  

* Develop experience with git workflow  
* Understand basic web architecture and how to extend a small web server  
* Use automated and manual testing to check progress  
